#include "CSerialInput.h"

#include <errno.h>      // Error number definitions
#include <stdint.h>     // C99 fixed data types
#include <stdio.h>      // Standard input/output definitions
#include <stdlib.h>     // C standard library
#include <string.h>     // String function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <termios.h>    // POSIX terminal control definitions
#include <ext/stdio_filebuf.h>

CSerialInput::CSerialInput()
{
    fd = this->open_port();
    setup();
    ready = false;
    line = "";
}

CSerialInput::~CSerialInput()
{
    close(fd);
}

bool CSerialInput::update(void)
{
    if (ready)
        line = "";

    ready = false;
    //write(fd, "a", 1);
    printf("read...");
    int size = read(fd, &buffer, 1);
    printf("done\n");
    if (size > 0)
    {
        if (buffer!= '\n')
            line.push_back(buffer);
        else
        {
            ready = true;
        }
    }
    return ready;
}

std::string CSerialInput::getReadyLine(void)
{
    return line;
}

int CSerialInput::open_port(void)
{
    int fd;    // File descriptor for the port
    fd = open("/dev/serial/by-id/usb-Arduino__www.arduino.cc__0042_5533031363535190D131-if00", O_RDWR | O_NOCTTY);
    //fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY);

    if (fd == -1)
    {
        fprintf(stderr, "open_port: Unable to open /dev/ttyUSB0 %s\n",strerror(errno));
        exit(EXIT_FAILURE);
    }

    return fd;
}

void CSerialInput::setup()
{
    int rc = 0;
    struct termios   options;    // Terminal options

    // Get the current options for the port
    if((rc = tcgetattr(fd, &options)) < 0)
    {
        fprintf(stderr, "failed to get attr: %d, %s\n", fd, strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Set the baud rates to 230400
    cfsetispeed(&options, B115200);

    // Set the baud rates to 230400
    cfsetospeed(&options, B115200);

    cfmakeraw(&options);
    options.c_cflag |= (CLOCAL | CREAD);   // Enable the receiver and set local mode
    options.c_cflag &= ~CSTOPB;            // 1 stop bit
    options.c_cflag &= ~CRTSCTS;           // Disable hardware flow control
    options.c_cc[VMIN]  = 1;
    options.c_cc[VTIME] = 2;

    // Set the new attributes
    if((rc = tcsetattr(fd, TCSANOW, &options)) < 0)
    {
        fprintf(stderr, "failed to set attr: %d, %s\n", fd, strerror(errno));
        exit(EXIT_FAILURE);
    }
}

void CSerialInput::waitToStart(void)
{
    while(1)
    {
    printf("wait,");
        if (update() && line.find("DMP"))
        {
            printf("found start line: %s\n", line.c_str());
            write(fd, "a", 1);
            break;
        }
    }
}

int cmain(void)
{
    CSerialInput input;
    input.waitToStart();
    printf("wait to start done\n");
    while(1)
    {
        if (input.update())
            printf("Line: %s\n", input.getReadyLine().c_str());
    }
    return EXIT_SUCCESS;
}
