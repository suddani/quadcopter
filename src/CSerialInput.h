#ifndef CSERIALINPUT_H
#define CSERIALINPUT_H

#include <string>

class CSerialInput
{
    public:
        CSerialInput();
        virtual ~CSerialInput();

        void waitToStart(void);

        bool update(void);
        std::string getReadyLine(void);
    protected:
        int fd;
        std::string line;

        char buffer;
        bool ready;

        int open_port(void);
        void setup();
    private:
};

#endif // CSERIALINPUT_H
