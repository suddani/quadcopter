#include "Controller.h"
#include "Copter_B.h"

Controller::Controller(Copter* c)
{
    copter = c;
    speed_yaw_goal = 0;

    K_p = 1.0;
    K_d = 1.0;
    K_i = 1.0;
}

Controller::~Controller()
{
    //dtor
}

/** @brief setYawSpeed
  *
  * @todo: document this function
  */
void Controller::setYawSpeed(const irr::f32& yaw)
{
    speed_yaw_goal = yaw;
}

/** @brief setUpSpeed
  *
  * @todo: document this function
  */
void Controller::setUpSpeed(const irr::f32& z)
{
    speed_goal.Y = z;
}

/** @brief setSpeed
  *
  * @todo: document this function
  */
void Controller::setSpeed(const irr::f32& x, const irr::f32& y)
{
    speed_goal.X = y;
    speed_goal.Z = x;
}

/** @brief setSpeedY
  *
  * @todo: document this function
  */
void Controller::setSpeedY(const irr::f32& y)
{
    speed_goal.X = y;
}

/** @brief setSpeedX
  *
  * @todo: document this function
  */
void Controller::setSpeedX(const irr::f32& x)
{
    speed_goal.Z = x;
}

/** @brief reset
  *
  * @todo: document this function
  */
void Controller::reset()
{
    speed_linear = irr::core::vector3df(0);
    rotation = irr::core::vector3df(0);
    copter->reset();
}

/** @brief collectSensorData
  *
  * @todo: document this function
  */
void Controller::collectSensorData(irr::f32 diff)
{
    if (!copter)
        return;
    SensorData sensors = copter->getSensorData();

    //Calculate rotation stuff
    speed_rotation = sensors.speed_rotation;

    irr::core::CMatrix4<irr::f32> mat;
    mat.setRotationDegrees(irr::core::vector3df(speed_rotation.X*diff*360.f/(2.f*irr::core::PI),speed_rotation.Y*diff*360.f/(2.f*irr::core::PI),speed_rotation.Z*diff*360.f/(2.f*irr::core::PI)));

    irr::core::CMatrix4<irr::f32> mat2;
    mat2.setRotationDegrees(rotation);

    rotation = (mat2*mat).getRotationDegrees();

    //calculate linear stuff
    acceleration_linear = sensors.acceleration_linear;
    speed_linear += acceleration_linear*diff;
}

/** @brief update
  *
  * @todo: document this function
  */
void Controller::update(irr::f32 diff)
{
    collectSensorData(diff);
    return;
    printf("Controller Speed: %f, %f, %f\n", speed_linear.X, speed_linear.Y, speed_linear.Z);
    printf("Controller Rotation: %f, %f, %f\n", rotation.X, rotation.Y, rotation.Z);
    printf("Controller Speed_rot: %f, %f, %f\n", speed_rotation.X, speed_rotation.Y, speed_rotation.Z);
    printf("Controller Acceleration: %f, %f, %f\n", acceleration_linear.X, acceleration_linear.Y, acceleration_linear.Z);
    printf("===============================\n");
}

