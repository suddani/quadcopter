#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <irrlicht.h>
class Copter;
class Controller
{
    public:
        Controller(Copter* c);
        virtual ~Controller();

        void setSpeed(const irr::f32& x, const irr::f32& y);
        void setSpeedX(const irr::f32& x);
        void setSpeedY(const irr::f32& y);
        void setUpSpeed(const irr::f32& z);
        void setYawSpeed(const irr::f32& yaw);

        void update(irr::f32 diff);

        void reset();
    protected:
        void collectSensorData(irr::f32 diff);

        Copter* copter;

        irr::core::vector3df speed_goal;
        irr::f32 speed_yaw_goal;

        irr::f32 K_p;
        irr::f32 K_d;
        irr::f32 K_i;

        irr::core::vector3df rotation;
        irr::core::vector3df speed_rotation;
        irr::core::vector3df speed_linear;

        irr::core::vector3df acceleration_linear;
        irr::core::vector3df acceleration_rotation;
    private:
};

#endif // CONTROLLER_H
