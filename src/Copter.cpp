#include "Copter.h"
#include <math.h>
const irr::f32 Copter::GRAVITY = 9.81f;


Copter::Copter(irr::scene::ISceneManager* smgr) : tensor(irr::core::matrix4::EM4CONST_IDENTITY)
{
    //ctor
    this->smgr = smgr;

    pos = smgr->addEmptySceneNode();

    yaw = smgr->addEmptySceneNode(pos);

    roll = smgr->addEmptySceneNode(yaw);

    pitch = smgr->addEmptySceneNode(roll);

    node = smgr->addEmptySceneNode(pitch);



    irr::scene::ISceneNode* tmp = smgr->addAnimatedMeshSceneNode(smgr->getMesh("LP_Apache.3ds"), node);
    tmp->setRotation(irr::core::vector3df(0,-90,0));
    tmp->setPosition(irr::core::vector3df(0,3,-1));
    tmp->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    tmp->setMaterialTexture(0, smgr->getVideoDriver()->getTexture("FinalApacheTexture.Png"));
/*
    irr::scene::ISceneNode* tmp = smgr->addCubeSceneNode(8);
    tmp->setScale(irr::core::vector3df(0.1,0.1,1));
    node->addChild(tmp);

    tmp = smgr->addCubeSceneNode(8);
    tmp->setScale(irr::core::vector3df(1,0.1,0.1));
    node->addChild(tmp);
*/
    pos->setPosition(irr::core::vector3df(0,70,0));

    motor_mass = 0.15f;
    mass = 0.5f+4*motor_mass; //kg
    radius = 0.4f; //m

    motor_thrust = 0.8f*Copter::GRAVITY; //Newton (0.8)

    //controlling the motor by giving different ammounts of power
    equilibrium = Copter::GRAVITY*mass/(4.f*motor_thrust);
    motor_power1 = equilibrium; //0.3125;
    motor_power2 = equilibrium;
    motor_power3 = equilibrium;
    motor_power4 = equilibrium;


    tensor(0,0) = 1.f;
    tensor(1,1) = 1.f;
    tensor(2,2) = 0.5f;


    t3 = 0;


    pos->grab();
}

Copter::~Copter()
{
    //dtor
    pos->remove();
    pos->drop();
}

/** @brief getLinearForce
  *
  * @todo: document this function
  */
irr::core::vector3df Copter::getLinearForce()
{
    irr::core::CMatrix4<irr::f32> mat;
    //mat.setRotationDegrees(node->getAbsoluteTransformation().getRotationDegrees());
    mat.setRotationDegrees(node->getRotation());

    irr::core::vector3df force(0,
                               motor_thrust*(motor_power1+motor_power2)+motor_thrust*(motor_power3+motor_power4)
                               , 0);

    mat.rotateVect(force);

    //force.normalize();
    //force *= motor_thrust*(motor_power1+motor_power2+motor_power3+motor_power4);

    irr::f32 multiplayer = (force.Y != 0 ? (Copter::GRAVITY+0.2)/force.Y : 1.0);
    //irr::f32 multiplayer = gravity/force.Y;



    //don't use more force than we have

    if (multiplayer*(motor_power1+motor_power2+motor_power3+motor_power4)>4)//3.2)
    {
        multiplayer = 4/(motor_power1+motor_power2+motor_power3+motor_power4);
        printf("Speed was to high\n");
    }
    else if (multiplayer<1)
    {
        printf("Speed was to low\n");
        multiplayer = 1;
    }

    printf("Had to scale force to keep hight by: %f\n", multiplayer);

    force *= multiplayer;

    //force.Y = force.Y-speed_linear.Y*2.f;


    printf("Force magnitude generated: %f max[%f]\n", force.getLength(), 4*motor_thrust);
    return force;
}

/** @brief reset
  *
  * @todo: document this function
  */
void Copter::reset()
{
    pos->setPosition(irr::core::vector3df(0,50,0));
    node->setRotation(irr::core::vector3df(0,0,0));
    pitch->setRotation(irr::core::vector3df(0,0,0));
    roll->setRotation(irr::core::vector3df(0,0,0));
    yaw->setRotation(irr::core::vector3df(0,0,0));
    speed_linear = irr::core::vector3df(0,0,0);
    speed_rot = irr::core::vector3df(0,0,0);
}


/** @brief getNode
  *
  * @todo: document this function
  */
irr::scene::ISceneNode* Copter::getNode()
{
    return node;
}

/** @brief checkMotorPower
  *
  * @todo: document this function
  */
void Copter::checkMotorPower(irr::f32& power)
{
    if (power < 0)
        power = 0;
    if (power > 1)
        power = 1;
}



//X
irr::core::vector3df Copter::getTorque1()
{
    if (irr::core::equals(motor_power1, motor_power2) && !irr::core::equals(speed_rot.X,0.0f))
    {
        //printf("Set to zero: %f == %f\n", motor_power1, motor_power2);
        motor_power1 += speed_rot.X/2.f;
        motor_power2 -= speed_rot.X/2.f;

        checkMotorPower(motor_power1);
        checkMotorPower(motor_power2);
    }

    irr::core::vector3df f1(0, motor_thrust*motor_power1,0);
    irr::core::vector3df f2(0, motor_thrust*motor_power2,0);
    irr::core::vector3df lever(0,0,radius);

    irr::core::vector3df t1 = f1.crossProduct(-lever)+f2.crossProduct(lever);

    return t1;
}


//Z
irr::core::vector3df Copter::getTorque2()
{
    if (irr::core::equals(motor_power3, motor_power4) && !irr::core::equals(speed_rot.Z, 0.0f))
    {
        //printf("Set to zero\n");
        motor_power3 -= speed_rot.Z/2.f;
        motor_power4 += speed_rot.Z/2.f;
        checkMotorPower(motor_power3);
        checkMotorPower(motor_power4);
    }

    irr::core::vector3df f3(0, motor_thrust*motor_power3,0);
    irr::core::vector3df f4(0, motor_thrust*motor_power4,0);
    irr::core::vector3df lever(radius,0,0);

    irr::core::vector3df t2 = f3.crossProduct(-lever)+f4.crossProduct(lever);

    return t2;
}

/** @brief getTorque3
  *
  * @todo: document this function
  */
irr::core::vector3df Copter::getTorque3()
{
    if (t3 == 0 && !irr::core::equals(speed_rot.Y, 0.0f))
    {
        return irr::core::vector3df(0, -speed_rot.Y/2.f, 0);
    }
    return irr::core::vector3df(0, t3, 0);
}



/** @brief motor
  *
  * @todo: document this function
  */
void Copter::motor(const int& i, const float& power)
{
    switch(i)
    {
    case 0:
        motor_power1 = power;
        checkMotorPower(motor_power1);
        break;
    case 1:
        motor_power2 = power;
        checkMotorPower(motor_power2);
        break;
    case 2:
        motor_power3 = power;
        checkMotorPower(motor_power3);
        break;
    case 3:
        motor_power4 = power;
        checkMotorPower(motor_power4);
        break;
    }
}

/** @brief getEqui
  *
  * @todo: document this function
  */
irr::f32 Copter::getEqui()
{
    return equilibrium;

}

/** @brief update_linear
  *
  * @todo: document this function
  */
void Copter::update_linear(const irr::f32& diff)
{
    acc_linear = (getLinearForce()+irr::core::vector3df(0,mass*-Copter::GRAVITY,0)-0.5*speed_linear)/mass;
    speed_linear += diff*acc_linear;

    pos->setPosition(pos->getPosition()+speed_linear*diff*10.f);
    pos->updateAbsolutePosition();
}

/** @brief update_rot
  *
  * @todo: document this function
  */
void Copter::update_rot(const irr::f32& diff)
{
    irr::core::vector3df torque = getTorque1()+getTorque2()+getTorque3();

    tensor.transformVect(torque);

    acc_rot = torque/(mass*radius*radius);

    speed_rot += acc_rot*diff;

    irr::core::CMatrix4<irr::f32> mat;
    mat.setRotationDegrees(irr::core::vector3df(speed_rot.X*diff*360.f/(2.f*irr::core::PI),speed_rot.Y*diff*360.f/(2.f*irr::core::PI),speed_rot.Z*diff*360.f/(2.f*irr::core::PI)));

    irr::core::CMatrix4<irr::f32> mat2;
    mat2.setRotationDegrees(node->getRotation());
    node->setRotation((mat2*mat).getRotationDegrees());

    pos->updateAbsolutePosition();
    node->updateAbsolutePosition();
}



/** @brief update
  *
  * @todo: document this function
  */
void Copter::update(const irr::f32& diff)
{
    irr::core::vector3df old_pos = pos->getPosition();

    update_linear(diff);

    irr::core::vector3df new_pos = pos->getPosition();

    update_rot(diff);

    irr::core::vector3df new_rot = node->getRotation();
    printf("Position: %f, %f, %f\n", new_pos.X/10.f, new_pos.Y/10.f, new_pos.Z/10.f);
    printf("Speed: %f, %f, %f\n", speed_linear.X, speed_linear.Y, speed_linear.Z);
    printf("Rotation: %f, %f, %f\n", new_rot.X, new_rot.Y, new_rot.Z);
    printf("Speed_rot: %f, %f, %f\n", speed_rot.X, speed_rot.Y, speed_rot.Z);
    printf("Acceleration: %f, %f, %f\n", acc_linear.X, acc_linear.Y, acc_linear.Z);
    printf("===============================\n");

    //printf("Torque: %f, %f, %f\n", torque_total.X, torque_total.Y, torque_total.Z);

    //collide with ground
    if (new_pos.Y <= 0.01)
    {
        if (speed_linear.Y < 0)
            speed_linear = irr::core::vector3df(0);
        pos->setPosition(irr::core::vector3df(old_pos.X, 0, old_pos.Z));
    }

    //set back motors

    motor_power1 = equilibrium;
    motor_power2 = equilibrium;
    motor_power3 = equilibrium;
    motor_power4 = equilibrium;

}

/** @brief getSensorData
  *
  * @todo: document this function
  */
SensorData Copter::getSensorData()
{
    return SensorData(acc_linear, speed_rot, 0.0);
}
