#ifndef COPTER_H
#define COPTER_H

#include <irrlicht.h>
#include "SensorData.h"
class Copter
{
    public:
        Copter(irr::scene::ISceneManager* smgr);
        virtual ~Copter();

        virtual void update(const irr::f32& diff);

        virtual void motor(const int& i, const float& power);
        virtual void motor_speed(const int& i, const float& w){}
        virtual irr::f32 getEqui();

        SensorData getSensorData();

        virtual void reset();

        irr::scene::ISceneNode* getNode();

        irr::f32 t3;

        static const irr::f32 GRAVITY;
    protected:
        virtual irr::core::vector3df getLinearForce();
        virtual irr::core::vector3df getTorque1();
        virtual irr::core::vector3df getTorque2();
        virtual irr::core::vector3df getTorque3();

        virtual void update_linear(const irr::f32& diff);
        virtual void update_rot(const irr::f32& diff);

        virtual void checkMotorPower(irr::f32& power);

        irr::scene::ISceneManager* smgr;
        irr::scene::ISceneNode* node;
        irr::scene::ISceneNode* pos;
        irr::scene::ISceneNode* pitch;
        irr::scene::ISceneNode* roll;
        irr::scene::ISceneNode* yaw;

        irr::core::vector3df acc_linear;
        irr::core::vector3df acc_rot;

        irr::core::vector3df speed_rot;
        irr::core::vector3df speed_linear;

        //Copter properties
        irr::f32 motor_mass;
        irr::f32 mass;
        irr::f32 radius;
        irr::core::CMatrix4<irr::f32> tensor;

        irr::f32 equilibrium;
        irr::f32 motor_thrust;

        irr::f32 motor_power1;
        irr::f32 motor_power2;
        irr::f32 motor_power3;
        irr::f32 motor_power4;

    private:
};

#endif // COPTER_H
