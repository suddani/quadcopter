#include "Copter_B.h"

Copter_B::Copter_B(irr::scene::ISceneManager* smgr) : Copter(smgr)
{
    //ctor
    w_max = 22.0827;
    k = motor_thrust/(w_max*w_max);

    w_1 = 0;
    w_2 = 0;
    w_3 = 0;
    w_4 = 0;

    w_1_target = 0;
    w_2_target = 0;
    w_3_target = 0;
    w_4_target = 0;
}

Copter_B::~Copter_B()
{
    //dtor
}

/** @brief getTorque3
  *
  * @todo: yaw
  */
irr::core::vector3df Copter_B::getTorque3()
{
    return irr::core::vector3df(0);
    return irr::core::vector3df(0,k*(w_1*w_1+w_2*w_2-w_3*w_3-w_4*w_4), 0);
}

/** @brief getTorque2
  *
  * @todo: roll
  */
irr::core::vector3df Copter_B::getTorque2()
{
    return irr::core::vector3df(0,0,radius*k*(w_3*w_3-w_4*w_4));
}

/** @brief getTorque1
  *
  * @todo: pitch
  */
irr::core::vector3df Copter_B::getTorque1()
{
    return irr::core::vector3df(radius*k*(-w_1*w_1+w_2*w_2), 0, 0);
}

/** @brief getLinearForce
  *
  * @todo: thrust
  */
irr::core::vector3df Copter_B::getLinearForce()
{
    irr::core::CMatrix4<irr::f32> mat;
    mat.setRotationDegrees(node->getAbsoluteTransformation().getRotationDegrees());

    irr::core::vector3df force(0,
                               1//motor_thrust*(motor_power1+motor_power2)+motor_thrust*(motor_power3+motor_power4)
                               , 0);

    mat.rotateVect(force);

    force.normalize();
    force *= k*(w_1*w_1+w_2*w_2+w_3*w_3+w_4*w_4);

    //force.Y = force.Y-speed_linear.Y*2.f;
    return force;
}

/** @brief getMaxMotorSpeed
  *
  * @todo: document this function
  */
const irr::f32 & Copter_B::getMaxMotorSpeed()
{
    return w_max;
}

/** @brief motor
  *
  * @todo: document this function
  */
void Copter_B::motor(const int& i, const float& power)
{
    float p = power;
    checkMotorPower(p);
    motor_speed(i, p*w_max);
}

/** @brief motor_speed
  *
  * @todo: the actual angular speed of each motor
  */
void Copter_B::motor_speed(const int& i, const float& w)
{
    switch(i)
    {
    case 0:
        w_1_target = w;
        checkMotorSpeed(w_1_target);
        break;
    case 1:
        w_2_target = w;
        checkMotorSpeed(w_2_target);
        break;
    case 2:
        w_3_target = w;
        checkMotorSpeed(w_3_target);
        break;
    case 3:
        w_4_target = w;
        checkMotorSpeed(w_4_target);
        break;
    }
}

/** @brief getEqui
  *
  * @todo: document this function
  */
irr::f32 Copter_B::getEqui()
{
    return 13.0f/w_max;
}


/** @brief checkMotorSpeed
  *
  * @todo: makes sure the motor doesn't spin to fast
  */
void Copter_B::checkMotorSpeed(irr::f32& w)
{
    if (w < 0)
        w = 0;
    if (w>w_max)
        w=w_max;
}

/** @brief update_motor_speed
  *
  * @todo: simulates motor speed change
  */
void Copter_B::update_motor_speed(irr::f32& w, const irr::f32& w_target, const irr::f32& diff)
{
    if (irr::core::equals(w, w_target))
        w = w_target;
    else
        w += (w_target-w)*diff*7.f;
}

/** @brief reset
  *
  * @todo: document this function
  */
void Copter_B::reset()
{
    Copter::reset();

    w_1 = 0;
    w_2 = 0;
    w_3 = 0;
    w_4 = 0;

    w_1_target = 0;
    w_2_target = 0;
    w_3_target = 0;
    w_4_target = 0;
}

/** @brief update
  *
  * @todo: document this function
  */
void Copter_B::update(const irr::f32& diff)
{
    update_motor_speed(w_1, w_1_target, diff);
    update_motor_speed(w_2, w_2_target, diff);
    update_motor_speed(w_3, w_3_target, diff);
    update_motor_speed(w_4, w_4_target, diff);

    printf("MotorSpeeds: %f %f %f %f\n", w_1, w_2, w_3, w_4);

    Copter::update(diff);
}



