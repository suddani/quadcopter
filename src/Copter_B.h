#ifndef COPTER_B_H
#define COPTER_B_H

#include "Copter.h"


class Copter_B : public Copter
{
    public:
        Copter_B(irr::scene::ISceneManager* smgr);
        virtual ~Copter_B();

        void motor(const int& i, const float& power);
        void motor_speed(const int& i, const float& w);
        irr::f32 getEqui();

        void update(const irr::f32& diff);
        void reset();

        const irr::f32& getMaxMotorSpeed();

    protected:
        virtual irr::core::vector3df getLinearForce();
        virtual irr::core::vector3df getTorque1();
        virtual irr::core::vector3df getTorque2();
        virtual irr::core::vector3df getTorque3();

        void checkMotorSpeed(irr::f32& w);
        void update_motor_speed(irr::f32& w, const irr::f32& w_target, const irr::f32& diff);


        irr::f32 k;
        irr::f32 w_max;

        irr::f32 w_1;
        irr::f32 w_1_target;
        irr::f32 w_2;
        irr::f32 w_2_target;
        irr::f32 w_3;
        irr::f32 w_3_target;
        irr::f32 w_4;
        irr::f32 w_4_target;
    private:
};

#endif // COPTER_B_H
