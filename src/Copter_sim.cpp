#include "Copter_sim.h"
#include "CSerialInput.h"
Copter_sim::Copter_sim(irr::scene::ISceneManager* smgr) : Copter(smgr)
{
    input = new CSerialInput(),
    input->waitToStart();
}

Copter_sim::~Copter_sim()
{
    delete input;
}

void Copter_sim::update(const irr::f32& diff)
{
    if (input->update())
    {
        std::string line = input->getReadyLine();
        printf("read line: %s\n", line.c_str());
        if (line[0] == 'y')
            update_rotation(line);
    }
}

void Copter_sim::update_rotation(std::string line)
{
    printf("update rotation\n");
    float y;
    float p;
    float r;

    sscanf(line.c_str(), "euler\t%f\t%f\t%f", &y, &p, &r);

    printf("Update to: %f, %f, %f\n", y,p,r);

    yaw->setRotation(irr::core::vector3df(0,y,0));
    pitch->setRotation(irr::core::vector3df(p,0,0));
    roll->setRotation(irr::core::vector3df(0,0,r));

    node->updateAbsolutePosition();
}
