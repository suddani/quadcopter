#ifndef COPTER_SIM_H
#define COPTER_SIM_H

#include "Copter.h"
#include <string>

class CSerialInput;
class Copter_sim : public Copter
{
    public:
        Copter_sim(irr::scene::ISceneManager* smgr);
        virtual ~Copter_sim();

        virtual void update(const irr::f32& diff);
    protected:
        CSerialInput* input;
        void update_rotation(std::string line);
    private:
};

#endif // COPTER_SIM_H
