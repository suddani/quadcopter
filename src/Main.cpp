#include "Main.h"
#include <irrlicht.h>
#include "Copter_B.h"
#include "Copter_sim.h"
#include "Controller.h"

Main::Main(irr::IrrlichtDevice* device, Copter* copter,Controller* controller)
{
    this->device = device;
    device->setEventReceiver(this);
    this->copter = copter;
    this->controller = controller;

    forward = false;
    back = false;
    right = false;
    left = false;


    irr::core::array<irr::SJoystickInfo> joystickInfo;
    device->activateJoysticks(joystickInfo);


    cam = device->getSceneManager()->addCameraSceneNode();
    //smgr->addCameraSceneNodeFPS(0,100,0.1);
}

Main::~Main()
{
    //dtor
}

/** @brief update
  *
  * @todo: document this function
  */
void Main::update(const irr::f32& diff)
{
    if (forward)
    {
        copter->motor(0, 0.1);
        copter->motor(1, 0.4);

        //controller->setSpeedX(2);
    }
    else if (back)
    {
        copter->motor(0, 0.4);
        copter->motor(1, 0.1);

        //controller->setSpeedX(-2);
    }

    if (left)
    {
        copter->motor(3, 0.1);
        copter->motor(2, 0.4);

        //controller->setSpeedY(2);
    }
    else if (right)
    {
        copter->motor(3, 0.4);
        copter->motor(2, 0.1);

        //controller->setSpeedY(-2);
    }

    if (JoystickState.IsButtonPressed(4))
    {
        copter->t3 = -0.1;
    }
    else if (JoystickState.IsButtonPressed(6))
    {
        copter->t3 = 0.1;
    }
    else
        copter->t3 = 0;

    irr::f32 factor = ((irr::f32)JoystickState.Axis[irr::SEvent::SJoystickEvent::AXIS_Y])/32767.f;

    //controller->setUpSpeed(((irr::f32)JoystickState.Axis[irr::SEvent::SJoystickEvent::AXIS_Y])/32767.f);

    copter->motor(0, copter->getEqui()+((irr::f32)JoystickState.Axis[irr::SEvent::SJoystickEvent::AXIS_Z])/32767.f*0.1f+factor);
    copter->motor(1, copter->getEqui()-((irr::f32)JoystickState.Axis[irr::SEvent::SJoystickEvent::AXIS_Z])/32767.f*0.1f+factor);

    copter->motor(2, copter->getEqui()-((irr::f32)JoystickState.Axis[irr::SEvent::SJoystickEvent::AXIS_R])/32767.f*0.1f+factor);
    copter->motor(3, copter->getEqui()+((irr::f32)JoystickState.Axis[irr::SEvent::SJoystickEvent::AXIS_R])/32767.f*0.1f+factor);

    copter->update(diff);
    updateCamera(diff);
}



/** @brief OnEvent
  *
  * @todo: document this function
  */
bool Main::OnEvent(const irr::SEvent& event)
{
    switch(event.EventType)
    {
    case irr::EET_KEY_INPUT_EVENT:
        switch(event.KeyInput.Key)
        {
        case irr::KEY_KEY_W:
            forward = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_S:
            back = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_D:
            right = event.KeyInput.PressedDown;
            break;
        case irr::KEY_KEY_A:
            left = event.KeyInput.PressedDown;
            break;
        case irr::KEY_SPACE:
            controller->reset();
            break;
        case irr::KEY_ESCAPE:
            device->closeDevice();
            return true;
            break;
        default:
            break;
        }
        break;
    case irr::EET_JOYSTICK_INPUT_EVENT:
        if (event.JoystickEvent.Joystick == 0)
        {
            JoystickState = event.JoystickEvent;
        }
    default:
        return false;
    }
    return false;
}

/** @brief updateCamera
  *
  * @todo: document this function
  */
void Main::updateCamera(const irr::f32& diff)
{
    irr::core::CMatrix4<irr::f32> yaw;
    yaw.setRotationDegrees(copter->getNode()->getAbsoluteTransformation().getRotationDegrees());


    irr::core::vector3df arm(0,0,-2);
    yaw.rotateVect(arm);
    irr::core::vector3df pos = copter->getNode()->getAbsolutePosition();
    cam->setTarget(pos-arm*5);
    cam->setPosition(pos+arm*10+irr::core::vector3df(0,10,0));
    cam->updateAbsolutePosition();
}






int main(int argc, char* argv[])
{
    irr::IrrlichtDevice* device = irr::createDevice(irr::video::EDT_OPENGL, irr::core::dimension2d<irr::u32>(800,600));
    irr::video::IVideoDriver* driver = device->getVideoDriver();
    irr::scene::ISceneManager* smgr = device->getSceneManager();

    //setup enviroment
    device->getFileSystem()->addFileArchive("map-20kdm2.pk3");
    irr::scene::IAnimatedMesh* mesh = smgr->getMesh("20kdm2.bsp");
    irr::scene::ISceneNode* node = smgr->addOctreeSceneNode(mesh->getMesh(0), 0, -1, 1024);
    node->setPosition(irr::core::vector3df(-1300,-144,-1249));

    //setup models
    Copter_sim c(smgr);
    //Controller controller(&c);
    Main m(device, &c, NULL);

    irr::u32 last_time = device->getTimer()->getTime();

    while (device->run())
    {
        driver->beginScene(true, true, irr::video::SColor(255,0,0,255));
        smgr->drawAll();
        driver->endScene();

        irr::u32 time = device->getTimer()->getTime();

        irr::f32 diff = (time-last_time)/1000.0f;
        last_time = time;


        m.update(diff);
        //controller.update(diff);


    }

    device->closeDevice();
    device->drop();
    return 0;
}
