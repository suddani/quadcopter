#ifndef MAIN_H
#define MAIN_H

#include <irrlicht.h>
class Copter;
class Controller;
class Main : public irr::IEventReceiver
{
    public:
        Main(irr::IrrlichtDevice* device, Copter* copter, Controller* controller);
        virtual ~Main();

        bool OnEvent(const irr::SEvent& event);

        void update(const irr::f32& diff);
    protected:
        void updateCamera(const irr::f32& diff);

        irr::IrrlichtDevice* device;

        irr::scene::ICameraSceneNode* cam;

        Copter* copter;
        Controller* controller;

        bool forward;
        bool back;
        bool left;
        bool right;

        irr::SEvent::SJoystickEvent JoystickState;
    private:
};

#endif // MAIN_H
