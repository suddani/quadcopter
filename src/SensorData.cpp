#include "SensorData.h"

SensorData::SensorData(const irr::core::vector3df& acceleration_linear_, const irr::core::vector3df& speed_rotation_, const irr::f32& proximity_)
{
    acceleration_linear = acceleration_linear_;
    speed_rotation = speed_rotation_;
    proximity = proximity_;
}

SensorData::~SensorData()
{
    //dtor
}
