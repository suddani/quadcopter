#ifndef SENSORDATA_H
#define SENSORDATA_H

#include <irrlicht.h>

class SensorData
{
    public:
        SensorData(const irr::core::vector3df& acceleration_linear_, const irr::core::vector3df& speed_rotation_, const irr::f32& proximity_);
        virtual ~SensorData();

        irr::core::vector3df acceleration_linear;
        irr::core::vector3df speed_rotation;

        irr::f32 proximity;
    protected:
    private:
};

#endif // SENSORDATA_H
