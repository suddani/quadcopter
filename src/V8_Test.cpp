#include "V8_Test.h"

V8_Test::V8_Test()
{
    //ctor
}

V8_Test::~V8_Test()
{
    //dtor
}

/** @brief simpleTest
  *
  * @todo: document this function
  */
void V8_Test::simpleTest()
{
    // Get the default Isolate created at startup.
    v8::Isolate* isolate = v8::Isolate::GetCurrent();

    // Create a stack-allocated handle scope.
    v8::HandleScope handle_scope(isolate);

    // Create a new context.
    v8::Persistent<v8::Context> context = v8::Context::New();

    // Enter the created context for compiling and
    // running the hello world script.
    v8::Context::Scope context_scope(context);

    // Create a string containing the JavaScript source code.
    v8::Handle<v8::String> source = v8::String::New("'Hello' + ', World!'");

    // Compile the source code.
    v8::Handle<v8::Script> script = v8::Script::Compile(source);

    // Run the script to get the result.
    v8::Handle<v8::Value> result = script->Run();

    // Dispose the persistent context.
    context.Dispose(isolate);

    // Convert the result to an ASCII string and print it.
    v8::String::AsciiValue ascii(result);
    printf("%s\n", *ascii);
}

v8::Handle<v8::Context> V8_Test::createContext()
{
    v8::HandleScope handle_scope;

    v8::Handle<v8::ObjectTemplate> global = v8::ObjectTemplate::New();
    global->Set(v8::String::New("print"), v8::FunctionTemplate::New(Print));
    global->Set(v8::String::New("load"), v8::FunctionTemplate::New(Load));
    global->Set(v8::String::New("require"), v8::FunctionTemplate::New(Load));
    global->Set(v8::String::New("include"), v8::FunctionTemplate::New(Load));

    return v8::Context::New(NULL, global);
}

/** @brief fileTest
  *
  * @todo: document this function
  */
void V8_Test::fileTest()
{

    v8::Handle<v8::Context> context = createContext();//v8::Context::New(NULL, global);
    v8::Context::Scope context_scope(context);

    v8::HandleScope handle_scope;

    // Compile the source code.
    v8::Handle<v8::Script> script = v8::Script::Compile(ReadFile("simple.js"));

    // Run the script to get the result.
    v8::Handle<v8::Value> result = script->Run();

    v8::String::AsciiValue ascii(result);
    printf("%s\n", *ascii);
}



// Reads a file into a v8 string.
v8::Handle<v8::String> V8_Test::ReadFile(const char* name)
{
  FILE* file = fopen(name, "rb");
  if (file == NULL) return v8::Handle<v8::String>();

  fseek(file, 0, SEEK_END);
  int size = ftell(file);
  rewind(file);

  char* chars = new char[size + 1];
  chars[size] = '\0';
  for (int i = 0; i < size;) {
    int read = static_cast<int>(fread(&chars[i], 1, size - i, file));
    i += read;
  }
  fclose(file);
  v8::Handle<v8::String> result = v8::String::New(chars, size);
  delete[] chars;
  return result;
}

v8::Handle<v8::Value> V8_Test::Print(const v8::Arguments& args)
{
  bool first = true;
  for (int i = 0; i < args.Length(); i++) {
    v8::HandleScope handle_scope(args.GetIsolate());
    if (first) {
      first = false;
    } else {
      printf(" ");
    }
    v8::String::Utf8Value str(args[i]);
    const char* cstr = ToCString(str);
    printf("%s", cstr);
  }
  printf("\n");
  fflush(stdout);
  return v8::Undefined();
}

v8::Handle<v8::Value> V8_Test::Load(const v8::Arguments& args)
{
  for (int i = 0; i < args.Length(); i++) {
    v8::HandleScope handle_scope(args.GetIsolate());
    v8::String::Utf8Value file(args[i]);
    if (*file == NULL) {
      return v8::ThrowException(v8::String::New("Error loading file"));
    }
    v8::Handle<v8::String> source = ReadFile(*file);
    if (source.IsEmpty()) {
      return v8::ThrowException(v8::String::New("Error loading file"));
    }
    if (!ExecuteString(args.GetIsolate(),
                       source,
                       v8::String::New(*file),
                       false,
                       false)) {
      return v8::ThrowException(v8::String::New("Error executing file"));
    }
  }
  return v8::Undefined();
}

// Executes a string within the current v8 context.
bool V8_Test::ExecuteString(v8::Isolate* isolate,
                   v8::Handle<v8::String> source,
                   v8::Handle<v8::Value> name,
                   bool print_result,
                   bool report_exceptions) {
  v8::HandleScope handle_scope(isolate);
  v8::TryCatch try_catch;
  v8::Handle<v8::Script> script = v8::Script::Compile(source, name);
  if (script.IsEmpty()) {
    // Print errors that happened during compilation.
    if (report_exceptions)
      ReportException(isolate, &try_catch);
    return false;
  } else {
    v8::Handle<v8::Value> result = script->Run();
    if (result.IsEmpty()) {
      //assert(try_catch.HasCaught());
      // Print errors that happened during execution.
      if (report_exceptions)
        ReportException(isolate, &try_catch);
      return false;
    } else {
      //assert(!try_catch.HasCaught());
      return true;
    }
  }
}

void V8_Test::ReportException(v8::Isolate* isolate, v8::TryCatch* try_catch)
{
  v8::HandleScope handle_scope(isolate);
  v8::String::Utf8Value exception(try_catch->Exception());
  const char* exception_string = ToCString(exception);
  v8::Handle<v8::Message> message = try_catch->Message();
  if (message.IsEmpty()) {
    // V8 didn't provide any extra information about this error; just
    // print the exception.
    fprintf(stderr, "%s\n", exception_string);
  } else {
    // Print (filename):(line number): (message).
    v8::String::Utf8Value filename(message->GetScriptResourceName());
    const char* filename_string = ToCString(filename);
    int linenum = message->GetLineNumber();
    fprintf(stderr, "%s:%i: %s\n", filename_string, linenum, exception_string);
    // Print line of source code.
    v8::String::Utf8Value sourceline(message->GetSourceLine());
    const char* sourceline_string = ToCString(sourceline);
    fprintf(stderr, "%s\n", sourceline_string);
    // Print wavy underline (GetUnderline is deprecated).
    int start = message->GetStartColumn();
    for (int i = 0; i < start; i++) {
      fprintf(stderr, " ");
    }
    int end = message->GetEndColumn();
    for (int i = start; i < end; i++) {
      fprintf(stderr, "^");
    }
    fprintf(stderr, "\n");
    v8::String::Utf8Value stack_trace(try_catch->StackTrace());
    if (stack_trace.length() > 0) {
      const char* stack_trace_string = ToCString(stack_trace);
      fprintf(stderr, "%s\n", stack_trace_string);
    }
  }
}

const char* V8_Test::ToCString(const v8::String::Utf8Value& value)
{
  return *value ? *value : "<string conversion failed>";
}



int bmain(int argc, char* argv[])
{
    V8_Test test;
    test.simpleTest();
    test.fileTest();
    return 0;
}
