#ifndef V8_TEST_H
#define V8_TEST_H
#include <v8.h>

class V8_Test
{
    public:
        V8_Test();
        virtual ~V8_Test();

        void simpleTest();
        void fileTest();

    protected:
        static v8::Handle<v8::String> ReadFile(const char* name);
        static const char* ToCString(const v8::String::Utf8Value& value);
        static v8::Handle<v8::Value> Print(const v8::Arguments& args);
        static v8::Handle<v8::Value> Load(const v8::Arguments& args);
        static bool ExecuteString(v8::Isolate* isolate,
                   v8::Handle<v8::String> source,
                   v8::Handle<v8::Value> name,
                   bool print_result,
                   bool report_exceptions);
        static void ReportException(v8::Isolate* isolate, v8::TryCatch* try_catch);

        static v8::Handle<v8::Context> createContext();
    private:
};

#endif // V8_TEST_H
